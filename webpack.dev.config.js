var defaultWebpackConfig = require("./webpack.config");
var path = require("path");
var BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

module.exports = Object.assign({}, defaultWebpackConfig, {

    mode: "development",
    // devtool: "source-map",

    // externals: [],

    devServer: {
        compress: true,
        contentBase: "./dist",
        host: "0.0.0.0",
        port: 9009,
    },

    plugins: [
        ...defaultWebpackConfig.plugins,
        new BundleAnalyzerPlugin({
            analyzerMode: "static",
            openAnalyzer: false,
            reportFilename: "webpack-bundle-report.html",
        }),
    ],
});
