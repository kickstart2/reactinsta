import { hot } from 'react-hot-loader/root'
import * as React from "react"

export interface AppProps
{
    compiler:string;
    framework:string;
}
const App = (props:AppProps) => <h1>App {props.compiler}  and {props.framework}</h1>;

export default hot(App)
