import App from "./components/App";
import * as React from "react";
import * as ReactDOM from "react-dom";

ReactDOM.render(
    <App compiler="TypeScript" framework="React" />,
    document.getElementById("app-holder")
);


